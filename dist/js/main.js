$(document).ready(function () {

    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots: false,
    responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $(".owl-carousel").owlCarousel();

    $(".burger-menu").on("click",function(){
        $("header ul").toggle("fast");
    });

    // scroll id
    $("menu a").click(function () {
        elementClick = $(this).attr("href");
        destination = $(elementClick).offset().top;
        $("body,html").animate({
            scrollTop: destination
        }, 800);
    });

    // slider
    $('.show-video').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<button class="slick-prev"> < </button>',
        nextArrow: '<button class="slick-next"> > </button>',
        cssEase: 'linear'
    });

    // fixed top navigation (scroll > page)
    //фиксируем шапку при скролле
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('nav').addClass("fixed-nav");
        } else {
            $('nav').removeClass("fixed-nav");
        }
    });

    $(window).resize(function () {
        if ($(window).width() < 1024) {
            // mobile menu open (burger-menu > click)
            //and link navigation click - mobile menu d-none
            $('.btn-open, .menu a').click(function () {
                $('.menu-collapse').toggleClass('d-lg-block').css('order', '1');
                $('.menu').toggleClass('menu-opened');
            });

            // burger animation click (focus-active)
            //появление стрелки
            $('.btn-open').on('click', function (e) {
                e.preventDefault;
                $(this).toggleClass('btn-open-active')
            });

            // burger animation click (focus-static)
            // скрываем стрелку
            $('.menu a').on('click', function (e) {
                e.preventDefault;
                $('.btn-open').removeClass('btn-open-active')
            });
        }
    });
});