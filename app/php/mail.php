<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

header("Content-Type: text/html; charset=utf-8");
$name = htmlspecialchars($_POST["name"]);
$mail = htmlspecialchars($_POST["mail"]);
$subject = htmlspecialchars($_POST["subject"]);

require_once(__DIR__ . '/bx_24/essence.php'); // Создание лида
$webHookScript  = 'https://name.bitrix24.ru/rest/10/euvhofctbc2l09ap/';
$eseence	    =   new essenceAdd($webHookScript);

$leadAdd = array(
    "TITLE"          => "(Сайт) $subject",
    "NAME"           =>  $name,
    "EMAIL"          => array(array("VALUE" => $mail))
);

$eseence->leadAdd($leadAdd);


$refferer = getenv('HTTP_REFERER');
$date=date("d.m.y"); // число.месяц.год  
$time=date("H:i"); // часы:минуты:секунды 
$myemail = "mail@yandex.ru"; // e-mail администратора

// Отправка письма администратору сайта
$tema = "$subject";
$message_to_myemail = "Лид:
Имя: $name<br>
Mail: $mail<br>
Источник (ссылка): $refferer
";

mail($myemail, $tema, $message_to_myemail, "From: BlACKWILL <mail@yandex.ru> \r\n Reply-To: \r\n"."MIME-Version: 1.0\r\n"."Content-type: text/html; charset=utf-8\r\n" );


// Сохранение инфо о лидах в файл leads.xls
$f = fopen("leads.xls", "a+");
fwrite($f," <tr>");    
fwrite($f," <td>$name</td> <td>$mail</td> <td>$date / $time</td>");
fwrite($f," <td>$refferer</td>");    
fwrite($f," </tr>");  
fwrite($f,"\n ");    
fclose($f);

?>