$(document).ready(function () {

    var year = new Date();
    $(".year").html(year.getFullYear());

    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots: false,
    responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $(".owl-carousel").owlCarousel();

    $(".burger-menu").on("click",function(){
        $("header ul").toggle("fast");
    });

    $('.button-up').click(function() {
        $('html, body').animate({scrollTop: 0},600);
        return false;
    });

    // scroll id
    $(".box-menu a").click(function () {
        var size = 100;
        elementClick = $(this).attr("href");
        destination = $(elementClick).offset().top - size;
        $("body,html").animate({
            scrollTop: destination
        }, 800);
    });

});